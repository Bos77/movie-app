import React from "react";
import { useSelector } from "react-redux";

import { MovieCard } from "../components/MovieCard";

export const MoviesContainer = ({ movieType }) => {
  const movieList = useSelector((state) => state[movieType]);
  const { genres } = useSelector((state) => state);

  return (
    <>
      {movieList.map((card) => {
        const {
          original_title,
          vote_average,
          poster_path,
          id,
          genre_ids,
          release_date,
        } = card;

        let genreFirst = "";
        if (genres.length > 0) {
          genreFirst = genres.filter((genre) => {
            return genre_ids.indexOf(genre.id) > -1;
          })[0].name;
        }

        return (
          <div className="movies__wrapper" key={id}>
            <MovieCard
              original_title={original_title}
              vote_average={vote_average.toPrecision(2)}
              poster_path={poster_path}
              genre={genreFirst}
              release_date={release_date.split("-")[0]}
              id={id}
            />
          </div>
        );
      })}
    </>
  );
};
