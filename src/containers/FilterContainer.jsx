import React from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  popularMovies,
  nowPlayingMovies,
  topRatedMovies,
} from "../store/actions/apiActions.js";

export const FilterContainer = () => {
  const dispatch = useDispatch();
  const movieTypeName = useSelector((state) => state.currentMovieType);

  return (
    <>
      <div className="gradient-border bg--gradient"></div>
      <section className="filter">
        <div className="container">
          <div className="filter__body">
            <button
              onClick={() =>
                dispatch(
                  popularMovies.trigger({
                    query: "movie/popular",
                    lang: true,
                    page: 1,
                  })
                )
              }
              className={`filter__option ${
                movieTypeName === "popular" ? "filter__option--active" : ""
              }`}
            >
              popular
            </button>
            <button
              onClick={() =>
                dispatch(
                  topRatedMovies.trigger({
                    query: "movie/top_rated",
                    lang: true,
                    page: 1,
                  })
                )
              }
              className={`filter__option ${
                movieTypeName === "top_rated" ? "filter__option--active" : ""
              }`}
            >
              top rated
            </button>
            <button
              onClick={() =>
                dispatch(
                  nowPlayingMovies.trigger({
                    query: "movie/now_playing",
                    lang: true,
                    page: 1,
                  })
                )
              }
              className={`filter__option ${
                movieTypeName === "now_playing" ? "filter__option--active" : ""
              }`}
            >
              now playing
            </button>
          </div>
        </div>
      </section>
    </>
  );
};
