import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  popularMovies,
  topRatedMovies,
  nowPlayingMovies,
  upcomingMovies,
} from "../store/actions/apiActions.js";

export const BtnContainer = () => {
  const movieTypeName = useSelector((state) => state.currentMovieType);
  const page = useSelector((state) => state.page);

  const dispatch = useDispatch();

  let routines = [
    popularMovies,
    topRatedMovies,
    nowPlayingMovies,
    upcomingMovies,
  ];

  let [currentRoutine] = routines.filter((route) =>
    route.TRIGGER.includes(movieTypeName.toUpperCase())
  );
  return (
    <button
      disabled={page > 5}
      onClick={() => {
        dispatch(
          currentRoutine.trigger({
            query: `movie/${movieTypeName}`,
            lang: true,
            page: page + 1,
          })
        );
      }}
      className={`movies__btn btn bg--gradient ${
        page > 5 ? "btn--disabled" : ""
      }`}
    >
      load
    </button>
  );
};
