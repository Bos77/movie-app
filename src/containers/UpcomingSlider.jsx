import React from "react";
import { useSelector } from "react-redux";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/swiper-bundle.css";

import { SectionHeading } from "../components/SectionHeading";
import { MovieCard } from "../components/MovieCard";

SwiperCore.use([Autoplay]);

export const UpcomingSlider = () => {
  const movieList = useSelector((state) => state.upcomingMovies);
  const { genres } = useSelector((state) => state);
  return (
    <section className="new-movies section-padding">
      <div className="container">
        <SectionHeading title={"Expected Premiere"} />
        <Swiper
          direction={"horizontal"}
          slidesPerView={6}
          spaceBetween={30}
          loop={true}
          speed={1000}
          autoplay={{
            delay: 1000,
            disableOnInteraction: false,
          }}
          breakpoints={{
            1250: {
              slidesPerView: 6,
            },
            991: {
              slidesPerView: 5,
            },
            767: {
              slidesPerView: 4,
            },
            575: {
              slidesPerView: 3,
            },
            350: {
              slidesPerView: 2,
            },
            10: {
              slidesPerView: 1,
              spaceBetween: 20,
            },
          }}
        >
          {movieList.map((card) => {
            const {
              original_title,
              vote_average,
              poster_path,
              id,
              genre_ids,
              release_date,
            } = card;

            let genreFirst = "";
            if (genres.length > 0) {
              genreFirst = genres.filter((genre) => {
                return genre_ids.indexOf(genre.id) > -1;
              })[0].name;
            }

            return (
              <SwiperSlide key={id}>
                <MovieCard
                  original_title={original_title}
                  vote_average={vote_average.toPrecision(2)}
                  poster_path={poster_path}
                  genre={genreFirst}
                  release_date={release_date.split("-")[0]}
                  id={id}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </section>
  );
};
