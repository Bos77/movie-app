import React, { useState } from "react";

import { Faq } from "../components/Faq";

export const FaqContainer = () => {
  const [visible, setVisible] = useState([false, false, false]);

  return (
    <ul className="qa__body">
      <Faq
        onClick={() => setVisible((prev) => [!prev[0], false, false])}
        active={visible[0]}
        question="Tempor metus est quam volutpat himenaeos bibendum?"
        answer="Pulvinar nisi ullamcorper odio cubilia nibh lacinia etiam pretium
              mollis egestas condimentum. Malesuada faucibus quam per nunc ut
              consequat magna imperdiet. Massa tristique porta mi elit lobortis
              risus."
      />
      <Faq
        onClick={() => setVisible((prev) => [false, !prev[1], false])}
        active={visible[1]}
        question="Tempor metus est quam volutpat himenaeos bibendum?"
        answer="Pulvinar nisi ullamcorper odio cubilia nibh lacinia etiam pretium
              mollis egestas condimentum. Malesuada faucibus quam per nunc ut
              consequat magna imperdiet. Massa tristique porta mi elit lobortis
              risus."
      />
      <Faq
        onClick={(event) => {
          console.log(event);
          setVisible((prev) => [false, false, !prev[2]]);
        }}
        active={visible[2]}
        question="Tempor metus est quam volutpat himenaeos bibendum?"
        answer="Pulvinar nisi ullamcorper odio cubilia nibh lacinia etiam pretium
              mollis egestas condimentum. Malesuada faucibus quam per nunc ut
              consequat magna imperdiet. Massa tristique porta mi elit lobortis
              risus."
      />
    </ul>
  );
};
