import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/swiper-bundle.css";

import { SectionHeading } from "../components/SectionHeading";

SwiperCore.use([Autoplay]);

export const PartnersSlider = () => {
  return (
    <section className="partners section-padding">
      <div className="container">
        <SectionHeading
          title={"Our Partners"}
          subtitle={
            "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using."
          }
        />
        <div className="partners__grid grid">
          <Swiper
            direction={"horizontal"}
            slidesPerView={6}
            spaceBetween={10}
            loop={true}
            speed={1000}
            autoplay={{
              delay: 1000,
              disableOnInteraction: false,
            }}
            breakpoints={{
              991: {
                slidesPerView: 5,
              },
              767: {
                slidesPerView: 4,
              },
              575: {
                slidesPerView: 3,
              },
              400: {
                slidesPerView: 2,
              },
              10: {
                slidesPerView: 1,
              },
            }}
          >
            <SwiperSlide>
              <img
                src={require("../assets/images/freecodecamp.png").default}
                alt="freecodecamp"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/simplilearn.png").default}
                alt="simplilearn"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/freecodecamp.png").default}
                alt="freecodecamp"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/simplilearn.png").default}
                alt="simplilearn"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/freecodecamp.png").default}
                alt="freecodecamp"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/simplilearn.png").default}
                alt="simplilearn"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                src={require("../assets/images/simplilearn.png").default}
                alt="simplilearn"
              />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </section>
  );
};
