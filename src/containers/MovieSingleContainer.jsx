import React from "react";
import { useSelector } from "react-redux";

import { MovieSingle } from "../components/MovieSingle";

export const MovieSingleContainer = () => {
  const movieInf = useSelector((state) => state.movieDetails);
  const {
    adult,
    original_title,
    vote_average,
    poster_path,
    genres,
    release_date,
    original_language,
    budget,
    overview,
    runtime,
    status,
  } = movieInf;
  return (
    <>
      {Object.keys(movieInf).length > 0 ? (
        <MovieSingle
          adult={adult}
          original_title={original_title}
          original_language={original_language}
          vote_average={vote_average.toPrecision(2)}
          poster_path={poster_path}
          release_date={release_date.split("-")[0]}
          budget={budget}
          overview={overview}
          runtime={`${parseInt(runtime / 60)}h ${runtime % 60}min`}
          status={status}
          genres={genres.map((item) => item.name).join(", ")}
        />
      ) : (
        ""
      )}
    </>
  );
};
