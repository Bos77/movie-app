import React from "react";
import { useSelector } from "react-redux";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/swiper-bundle.css";

import { SliderCard } from "../components/SliderCard";

SwiperCore.use([Autoplay]);

export const MainSlider = () => {
  const { upcomingMovies } = useSelector((state) => state);

  return (
    <section className="slider-movie">
      <div className="container">
        <Swiper
          direction={"horizontal"}
          slidesPerView={1}
          spaceBetween={100}
          loop={true}
          speed={1000}
          autoplay={{
            delay: 2000,
            disableOnInteraction: false,
          }}
        >
          {upcomingMovies.map((card) => {
            const {
              original_title,
              vote_average,
              adult,
              poster_path,
              release_date,
              overview,
              id,
            } = card;
            return (
              <SwiperSlide key={id}>
                <SliderCard
                  original_title={original_title}
                  vote_average={vote_average.toPrecision(2)}
                  adult={adult}
                  poster_path={poster_path}
                  release_date={release_date.split("-")[0]}
                  overview={`${overview.split(" ").slice(0, 30).join(" ")}...`}
                  id={id}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </section>
  );
};
