import React from "react";
import { Link } from "react-router-dom";

export const SliderCard = ({
  original_title,
  vote_average,
  adult,
  poster_path,
  release_date,
  overview,
  id,
}) => {
  return (
    <div className="slider-movie__grid grid">
      <div className="slider-movie__content">
        <h1 className="slider-movie__title title">{original_title}</h1>
        <div className="slider-movie__inf">
          <span className="slider-movie__date">{release_date}</span>
          <span className="movie__age gradient--text">{adult ? 18 : 12}+</span>
          <span className="movie__rate">
            {vote_average}
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M22,9.67A1,1,0,0,0,21.14,9l-5.69-.83L12.9,3a1,1,0,0,0-1.8,0L8.55,8.16,2.86,9a1,1,0,0,0-.81.68,1,1,0,0,0,.25,1l4.13,4-1,5.68A1,1,0,0,0,6.9,21.44L12,18.77l5.1,2.67a.93.93,0,0,0,.46.12,1,1,0,0,0,.59-.19,1,1,0,0,0,.4-1l-1-5.68,4.13-4A1,1,0,0,0,22,9.67Zm-6.15,4a1,1,0,0,0-.29.88l.72,4.2-3.76-2a1.06,1.06,0,0,0-.94,0l-3.76,2,.72-4.2a1,1,0,0,0-.29-.88l-3-3,4.21-.61a1,1,0,0,0,.76-.55L12,5.7l1.88,3.82a1,1,0,0,0,.76.55l4.21.61Z"></path>
            </svg>
          </span>
        </div>
        <p className="slider-movie__descr">{overview}</p>
        <Link to={`movie/${id}`} className="slider-movie__btn btn bg--gradient">
          View
        </Link>
      </div>
      <div className="slider-movie__image">
        <img
          src={`https://image.tmdb.org/t/p/w300${poster_path}`}
          alt="movie"
        />
      </div>
    </div>
  );
};
