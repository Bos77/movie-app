import React from "react";
import { Link } from "react-router-dom";

export const MovieCard = ({
  original_title,
  vote_average,
  poster_path,
  genre,
  release_date,
  id,
}) => {
  return (
    <Link to={`/movie/${id}`} className="movies__card">
      <div className="movies__img">
        <img
          src={`https://image.tmdb.org/t/p/w300${poster_path}`}
          alt="movie"
        />
        <span className="movies__rate">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path d="M22,9.67A1,1,0,0,0,21.14,9l-5.69-.83L12.9,3a1,1,0,0,0-1.8,0L8.55,8.16,2.86,9a1,1,0,0,0-.81.68,1,1,0,0,0,.25,1l4.13,4-1,5.68A1,1,0,0,0,6.9,21.44L12,18.77l5.1,2.67a.93.93,0,0,0,.46.12,1,1,0,0,0,.59-.19,1,1,0,0,0,.4-1l-1-5.68,4.13-4A1,1,0,0,0,22,9.67Zm-6.15,4a1,1,0,0,0-.29.88l.72,4.2-3.76-2a1.06,1.06,0,0,0-.94,0l-3.76,2,.72-4.2a1,1,0,0,0-.29-.88l-3-3,4.21-.61a1,1,0,0,0,.76-.55L12,5.7l1.88,3.82a1,1,0,0,0,.76.55l4.21.61Z"></path>
          </svg>
          {vote_average}
        </span>
        <svg
          version="1.1"
          id="Capa_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 60 60"
        >
          <g>
            <path
              d="M45.563,29.174l-22-15c-0.307-0.208-0.703-0.231-1.031-0.058C22.205,14.289,22,14.629,22,15v30
         c0,0.371,0.205,0.711,0.533,0.884C22.679,45.962,22.84,46,23,46c0.197,0,0.394-0.059,0.563-0.174l22-15
         C45.836,30.64,46,30.331,46,30S45.836,29.36,45.563,29.174z M24,43.107V16.893L43.225,30L24,43.107z"
            />
            <path
              d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M30,58C14.561,58,2,45.439,2,30
         S14.561,2,30,2s28,12.561,28,28S45.439,58,30,58z"
            />
          </g>
        </svg>
      </div>
      <h2 className="movies__title">{original_title}</h2>
      <div className="movies__inf">
        <span className="movies__inf-item">Free</span>
        <span className="movies__inf-item">{genre}</span>
        <span className="movies__inf-item">{release_date}</span>
      </div>
    </Link>
  );
};
