import React from "react";

export const PageHeading = ({ page }) => {
  return (
    <>
      <section className="page-heading">
        <div className="container">
          <div className="page-heading__grid grid">
            <h1 className="page-heading__title">{page}</h1>
            <span className="page-heading__breadcrumb">
              Home
              <svg
                width="24"
                height="24"
                xmlns="http://www.w3.org/2000/svg"
                fillRule="evenodd"
                clipRule="evenodd"
              >
                <path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z" />
              </svg>
              {page}
            </span>
          </div>
        </div>
      </section>
      <div className="gradient-border bg--gradient"></div>
    </>
  );
};
