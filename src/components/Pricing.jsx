import React from "react";

export const Pricing = () => {
  return (
    <section className="pricing section-padding">
      <div className="container">
        <div className="heading">
          <h1 className="heading__title">Select Your Plan</h1>
          <p className="heading__subtitle heading__subtitle--big">
            No hidden fees, equipment rentals, or installation appointments.
          </p>
        </div>
        <div className="pricing__grid grid">
          <div className="pricing__wrapper">
            <article className="pricing__card border--gradient">
              <div className="pricing__heading">
                <h2 className="pricing__type">Basic</h2>
                <span className="pricing__price gradient--text">Free</span>
              </div>
              <ul className="pricing__features">
                <li className="pricing__option">7 days</li>
                <li className="pricing__option">720p Resolution</li>
                <li className="pricing__option">Limited Availability</li>
                <li className="pricing__option">Desktop Only</li>
                <li className="pricing__option">Limited Support</li>
              </ul>
              <button className="pricing__btn btn bg--gradient">
                choose plan
              </button>
            </article>
          </div>
          <div className="pricing__wrapper">
            <article className="pricing__card border--gradient">
              <div className="pricing__heading">
                <h2 className="pricing__type">Basic</h2>
                <span className="pricing__price gradient--text">Free</span>
              </div>
              <ul className="pricing__features">
                <li className="pricing__option">7 days</li>
                <li className="pricing__option">720p Resolution</li>
                <li className="pricing__option">Limited Availability</li>
                <li className="pricing__option">Desktop Only</li>
                <li className="pricing__option">Limited Support</li>
              </ul>
              <button className="pricing__btn btn bg--gradient">
                choose plan
              </button>
            </article>
          </div>
          <div className="pricing__wrapper">
            <article className="pricing__card border--gradient">
              <div className="pricing__heading">
                <h2 className="pricing__type">Basic</h2>
                <span className="pricing__price gradient--text">Free</span>
              </div>
              <ul className="pricing__features">
                <li className="pricing__option">7 days</li>
                <li className="pricing__option">720p Resolution</li>
                <li className="pricing__option">Limited Availability</li>
                <li className="pricing__option">Desktop Only</li>
                <li className="pricing__option">Limited Support</li>
              </ul>
              <button className="pricing__btn btn bg--gradient">
                choose plan
              </button>
            </article>
          </div>
        </div>
      </div>
    </section>
  );
};
