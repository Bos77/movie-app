import React from "react";

export const SectionHeading = ({ title, subtitle }) => {
  return (
    <div className="heading">
      <h1 className="heading__title">{title}</h1>
      <p className="heading__subtitle heading__subtitle--big">{subtitle}</p>
    </div>
  );
};
