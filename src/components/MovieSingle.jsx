import React from "react";

export const MovieSingle = ({
  adult,
  original_title,
  vote_average,
  poster_path,
  genres,
  release_date,
  original_language,
  budget,
  overview,
  runtime,
  status,
}) => {
  return (
    <section className="details section-padding">
      <div className="container">
        <div className="details__body grid">
          <div className="details__img">
            <img
              src={`https://image.tmdb.org/t/p/w300${poster_path}`}
              alt="movie"
            />
          </div>
          <div className="details__content">
            <div className="details__inf">
              <span className="movie__rate">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <path d="M22,9.67A1,1,0,0,0,21.14,9l-5.69-.83L12.9,3a1,1,0,0,0-1.8,0L8.55,8.16,2.86,9a1,1,0,0,0-.81.68,1,1,0,0,0,.25,1l4.13,4-1,5.68A1,1,0,0,0,6.9,21.44L12,18.77l5.1,2.67a.93.93,0,0,0,.46.12,1,1,0,0,0,.59-.19,1,1,0,0,0,.4-1l-1-5.68,4.13-4A1,1,0,0,0,22,9.67Zm-6.15,4a1,1,0,0,0-.29.88l.72,4.2-3.76-2a1.06,1.06,0,0,0-.94,0l-3.76,2,.72-4.2a1,1,0,0,0-.29-.88l-3-3,4.21-.61a1,1,0,0,0,.76-.55L12,5.7l1.88,3.82a1,1,0,0,0,.76.55l4.21.61Z"></path>
                </svg>
                {vote_average}
              </span>
              <span className="movie__age gradient--text">
                {adult ? "18" : "12"}+
              </span>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Title:</h2>
              <p className="details__value gradient--text">{original_title}</p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Genres:</h2>
              <p className="details__value gradient--text">{genres}</p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Release year:</h2>
              <p className="details__value gradient--text">{release_date}</p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Status:</h2>
              <p className="details__value gradient--text">{status}</p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Running time:</h2>
              <p className="details__value gradient--text">{runtime}</p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Language:</h2>
              <p className="details__value gradient--text">
                {original_language}
              </p>
            </div>
            <div className="details__inf">
              <h2 className="details__title">Budget:</h2>
              <p className="details__value gradient--text">{budget}$</p>
            </div>
            <p className="details__descr">{overview}</p>
          </div>
        </div>
      </div>
    </section>
  );
};
