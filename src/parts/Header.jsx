import React, { useState } from "react";
import { Link } from "react-router-dom";

import Logo from "../assets/images/Logo.png";

export const Header = () => {
  const [burgerVisible, setBurgerVisible] = useState(false);
  window.addEventListener("click", (e) => {
    setBurgerVisible(false);
  });

  return (
    <>
      <header className="header">
        <div className="container">
          <div className="header__body">
            <Link to="/" className="header__logo">
              <img src={Logo} alt="Logo" />
            </Link>
            <nav className="nav">
              <Link to="/" className="nav__link">
                Home
              </Link>
              <Link to="/movies" className="nav__link">
                Catalog
              </Link>
              <Link to="/pricing" className="nav__link">
                Pricing Plans
              </Link>
              <Link to="/about-us" className="nav__link">
                About us
              </Link>
              <Link to="/contacts" className="nav__link">
                Contacts
              </Link>
            </nav>
            <div className="header__buttons">
              <svg
                onClick={(e) => {
                  e.stopPropagation();
                  setBurgerVisible(true);
                }}
                height="384pt"
                viewBox="0 -53 384 384"
                width="384pt"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"></path>
                <path d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"></path>
                <path d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"></path>
              </svg>
              <button className="header__sign btn bg--gradient">
                <span>sign in</span>
                <svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg">
                  <title />
                  <g>
                    <path d="M43.7578,61.7578a5.9994,5.9994,0,1,0,8.4844,8.4844l18-18a5.9979,5.9979,0,0,0,0-8.4844l-18-18a5.9994,5.9994,0,0,0-8.4844,8.4844L51.5156,42H6A6,6,0,0,0,6,54H51.5156Z" />
                    <path d="M90,0H30a5.9966,5.9966,0,0,0-6,6V18a6,6,0,0,0,12,0V12H84V84H36V78a6,6,0,0,0-12,0V90a5.9966,5.9966,0,0,0,6,6H90a5.9966,5.9966,0,0,0,6-6V6A5.9966,5.9966,0,0,0,90,0Z" />
                  </g>
                </svg>
              </button>
            </div>
          </div>
        </div>
        <div className={`burger ${burgerVisible ? "burger--active" : ""}`}>
          <div className="burger__heading">
            <img src={Logo} alt="Logo" />
            <svg
              onClick={(e) => {
                e.stopPropagation();
                setBurgerVisible(false);
              }}
              className="burger__close"
              height="329pt"
              viewBox="0 0 329.26933 329"
              width="329pt"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"></path>
            </svg>
          </div>
          <div className="gradient-border bg--gradient"></div>
          <Link to="/" className="nav__link">
            Home
          </Link>
          <Link to="/movies" className="nav__link">
            Catalog
          </Link>
          <Link to="/pricing" className="nav__link">
            Pricing Plans
          </Link>
          <Link to="/about-us" className="nav__link">
            About us
          </Link>
          <Link to="/contacts" className="nav__link">
            Contacts
          </Link>
        </div>
      </header>
      <div className="header__bt-border gradient-border bg--gradient"></div>
    </>
  );
};
