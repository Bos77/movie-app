import React from "react";

import { Header } from "./parts/Header";
import { Footer } from "./parts/Footer";
import { PageRoutes } from "./routes";

export const App = () => {
  return (
    <>
      <Header />
      <main className="main">
        <PageRoutes />
      </main>
      <Footer />
    </>
  );
};
