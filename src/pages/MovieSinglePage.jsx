import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";

import { scrollToTop } from "../services/helpers/scrollToTop.js";
import {
  movieDetails,
  recommendedMovies,
} from "../store/actions/apiActions.js";
import { MovieSingleContainer } from "../containers/MovieSingleContainer";
import { MoviesContainer } from "../containers/MoviesContainer";
import { SectionHeading } from "../components/SectionHeading";

export const MovieSinglePage = () => {
  scrollToTop();

  const { id } = useParams();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(movieDetails.trigger({ query: `movie/${id}`, lang: true }));
    dispatch(
      recommendedMovies.trigger({
        query: `movie/${id}/similar`,
        lang: true,
        page: 1,
      })
    );
  });

  return (
    <>
      <MovieSingleContainer />
      <div className="gradient-border bg--gradient"></div>
      <section className="movies section-padding">
        <div className="container">
          <SectionHeading title="You may also like" />
          <div className="movies__grid grid">
            <MoviesContainer movieType="recommendedMovies" />
          </div>
        </div>
      </section>
    </>
  );
};
