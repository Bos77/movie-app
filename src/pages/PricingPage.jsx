import React from "react";

import { scrollToTop } from "../services/helpers/scrollToTop.js";
import { PartnersSlider } from "../containers/PartnersSlider";
import { PageHeading } from "../components/PageHeading";
import { Pricing } from "../components/Pricing";
import { Features } from "../components/Features";

export const PricingPage = () => {
  scrollToTop();

  return (
    <>
      <PageHeading page="Pricing" />
      <Pricing />
      <Features />
      <PartnersSlider />
    </>
  );
};
