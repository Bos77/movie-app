import React from "react";

import { scrollToTop } from "../services/helpers/scrollToTop.js";
import { PartnersSlider } from "../containers/PartnersSlider";
import { FaqContainer } from "../containers/FaqContainer";
import { PageHeading } from "../components/PageHeading";
import { Features } from "../components/Features";
import { SectionHeading } from "../components/SectionHeading";

export const AboutUsPage = () => {
  scrollToTop();

  return (
    <>
      <PageHeading page="About Us" />
      <section className="about-us section-padding">
        <div className="container">
          <div className="about-us__heading">
            <h1 className="heading__title heading__title--big">
              MboxTV – Best place for movies
            </h1>
            <p className="heading__subtitle">
              Many desktop publishing packages and web page editors now use
              Lorem Ipsum as their default model text, and a search for 'lorem
              ipsum' will uncover many web sites still in their infancy. Various
              versions have evolved over the years, sometimes by accident,
              sometimes on purpose (injected humour and the like).
            </p>
            <p className="heading__subtitle">
              All the Lorem Ipsum generators on the Internet tend to repeat
              predefined chunks as necessary, making this the first true
              generator on the Internet. It uses a dictionary of over 200 Latin
              words, combined with a handful of model sentence structures, to
              generate Lorem Ipsum which looks reasonable. The generated Lorem
              Ipsum is therefore always free from repetition, injected humour,
              or non-characteristic words etc.
            </p>
          </div>
          <div className="steps grid">
            <div className="steps__wrapper">
              <article className="steps__card">
                <div className="features__heading">
                  <span className="steps__number gradient--text">01</span>
                  <h2 className="features__title">Choose your Plan</h2>
                </div>
                <p className="heading__subtitle">
                  It to make a type specimen book. It has survived not only five
                  centuries, but also the leap into electronic typesetting,
                  remaining
                </p>
              </article>
            </div>
            <div className="steps__wrapper">
              <article className="steps__card">
                <div className="features__heading">
                  <span className="steps__number gradient--text">02</span>
                  <h2 className="features__title">Create an account</h2>
                </div>
                <p className="heading__subtitle">
                  All the Lorem Ipsum generators on the Internet tend to repeat
                  predefined chunks as necessary, making this the first
                </p>
              </article>
            </div>
            <div className="steps__wrapper">
              <article className="steps__card">
                <div className="features__heading">
                  <span className="steps__number gradient--text">03</span>
                  <h2 className="features__title">Enjoy MboxTV</h2>
                </div>
                <p className="heading__subtitle">
                  It to make a type specimen book. It has survived not only five
                  centuries, but also the leap into electronic typesetting
                </p>
              </article>
            </div>
          </div>
        </div>
      </section>
      <Features />
      <PartnersSlider />
      <section className="qa section-padding">
        <div className="container">
          <SectionHeading title="FAQ" />
          <FaqContainer />
        </div>
      </section>
    </>
  );
};
