import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { scrollToTop } from "../services/helpers/scrollToTop";
import { popularMovies, upcomingMovies } from "../store/actions/apiActions.js";
import { FilterContainer } from "../containers/FilterContainer";
import { MoviesContainer } from "../containers/MoviesContainer";
import { UpcomingSlider } from "../containers/UpcomingSlider";
import { BtnContainer } from "../containers/BtnContainer";
import { PageHeading } from "../components/PageHeading";

export const MoviesPage = () => {
  const currentM = useSelector((state) => state.currentMovies);
  if (currentM.length < 21) scrollToTop();

  const upcomingM = useSelector((state) => state.upcomingMovies);

  const dispatch = useDispatch();
  useEffect(() => {
    if (currentM.length === 0 && upcomingM.length === 0) {
      dispatch(
        popularMovies.trigger({
          query: "movie/popular",
          lang: true,
          page: 1,
        })
      );
      dispatch(
        upcomingMovies.trigger({ query: "movie/upcoming", lang: true, page: 1 })
      );
    }
  }, []);
  return (
    <>
      <PageHeading page="Catalog" />
      <FilterContainer />
      <section className="movies section-padding">
        <div className="container">
          <div className="movies__grid grid">
            <MoviesContainer movieType="currentMovies" />
          </div>
          <BtnContainer />
        </div>
      </section>
      <UpcomingSlider />
    </>
  );
};
