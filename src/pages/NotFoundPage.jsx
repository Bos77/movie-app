import React from "react";
import { useNavigate } from "react-router-dom";

export const NotFoundPage = ({ errorNum, errorMessage }) => {
  const navigate = useNavigate();
  return (
    <section className="page-404">
      <div className="page-404__wrapper">
        <div className="page-404__body border--gradient">
          <h1 className="page-404__title gradient--text">{errorNum}</h1>
          <p className="page-404__descr heading__subtitle">{errorMessage}</p>
          <button
            onClick={() => navigate(-1)}
            className="page-404__btn btn bg--gradient"
          >
            go back
          </button>
        </div>
      </div>
    </section>
  );
};
