import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { scrollToTop } from "../services/helpers/scrollToTop.js";
import {
  popularMovies,
  trendingMovies,
  upcomingMovies,
  genres,
} from "../store/actions/apiActions.js";
import { MainSlider } from "../containers/MainSlider";
import { FilterContainer } from "../containers/FilterContainer";
import { MoviesContainer } from "../containers/MoviesContainer";
import { UpcomingSlider } from "../containers/UpcomingSlider";
import { PartnersSlider } from "../containers/PartnersSlider.jsx";
import { Pricing } from "../components/Pricing";

export const HomePage = () => {
  scrollToTop();

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(trendingMovies.trigger({ query: "trending/movie/day" }));
    dispatch(
      popularMovies.trigger({ query: "movie/popular", lang: true, page: 1 })
    );
    dispatch(
      upcomingMovies.trigger({ query: "movie/upcoming", lang: true, page: 1 })
    );
    dispatch(genres.trigger({ query: "genre/movie/list", lang: true }));
  }, []);

  return (
    <>
      <MainSlider />
      <FilterContainer />
      <section className="movies section-padding">
        <div className="container">
          <div className="movies__grid grid">
            <MoviesContainer movieType="currentMovies" />
          </div>
        </div>
      </section>
      <UpcomingSlider />
      <Pricing />
      <PartnersSlider />
    </>
  );
};
