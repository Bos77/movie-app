import React from "react";
import { Route, Routes } from "react-router-dom";

import { HomePage } from "./pages/HomePage";
import { MoviesPage } from "./pages/MoviesPage";
import { AboutUsPage } from "./pages/AboutUsPage";
import { PricingPage } from "./pages/PricingPage";
import { ContactsPage } from "./pages/ContactsPage";
import { MovieSinglePage } from "./pages/MovieSinglePage";
import { NotFoundPage } from "./pages/NotFoundPage";

export const PageRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/movies" element={<MoviesPage />} />
      <Route path="/pricing" element={<PricingPage />} />
      <Route path="/about-us" element={<AboutUsPage />} />
      <Route path="/contacts" element={<ContactsPage />} />
      <Route path="/movie/:id" element={<MovieSinglePage />} />
      <Route
        path="*"
        element={
          <NotFoundPage
            errorNum="404"
            errorMessage="The page you are looking for not available! "
          />
        }
      />
    </Routes>
  );
};
