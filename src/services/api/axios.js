import axios from "axios";

export const request = axios.create({
  method: "get",
  baseURL: "https://cors-anywhere.herokuapp.com/https://api.themoviedb.org/3",
  headers: { "X-Custom-Header": "Burxon movie app" },
});
