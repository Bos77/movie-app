export const queryBuilder = ({ query, lang, page }) => {
  let querymodifier;
  if (page && lang) {
    querymodifier = `/${query}?api_key=9376afa3518e27a8d01680806bd2ab33&language=en-US&page=${page}`;
  } else if (lang) {
    querymodifier = `/${query}?api_key=9376afa3518e27a8d01680806bd2ab33&language=en-US`;
  } else {
    querymodifier = `/${query}?api_key=9376afa3518e27a8d01680806bd2ab33`;
  }
  return querymodifier;
};
