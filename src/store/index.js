import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

import { apiReducer } from "./reducers/apiReducer";
import { rootSaga } from "./saga/fetchData";

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
  apiReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);
