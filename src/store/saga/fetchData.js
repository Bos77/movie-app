import { call, put, takeLatest } from "redux-saga/effects";

import { routineModifier } from "../../services/helpers/routineModifier";
import { queryBuilder } from "../../services/helpers/queryBuilder";
import { request } from "../../services/api/axios";

import {
  topRatedMovies,
  nowPlayingMovies,
  popularMovies,
  trendingMovies,
  upcomingMovies,
  genres,
  movieDetails,
  recommendedMovies,
} from "../actions/apiActions";

const routes = [
  topRatedMovies,
  nowPlayingMovies,
  popularMovies,
  trendingMovies,
  upcomingMovies,
  genres,
  movieDetails,
  recommendedMovies,
];

function* apiWorker({ type, payload }) {
  try {
    const data = yield call(request, queryBuilder(payload));

    let [currentRoute] = routes.filter((item) => {
      return item.SUCCESS === routineModifier(type)[1];
    });

    let payloadData;
    if (Object.keys(data.data).length < 20) {
      [payloadData] = Object.entries(data.data).filter(([key, value]) => {
        return Array.isArray(value);
      });
      payloadData = payloadData[1];
    } else {
      payloadData = data.data;
    }
    yield put(currentRoute.success(payloadData));
  } catch (error) {
    yield put({ type: routineModifier(type)[2], payload: error });
    console.log(error);
  }
}

export function* rootSaga() {
  yield takeLatest(topRatedMovies.TRIGGER, apiWorker);
  yield takeLatest(nowPlayingMovies.TRIGGER, apiWorker);
  yield takeLatest(popularMovies.TRIGGER, apiWorker);
  yield takeLatest(trendingMovies.TRIGGER, apiWorker);
  yield takeLatest(upcomingMovies.TRIGGER, apiWorker);
  yield takeLatest(genres.TRIGGER, apiWorker);
  yield takeLatest(movieDetails.TRIGGER, apiWorker);
  yield takeLatest(recommendedMovies.TRIGGER, apiWorker);
}
