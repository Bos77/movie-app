import { createRoutine } from "redux-saga-routines";

export const topRatedMovies = createRoutine("TOP_RATED_MOVIES", {
  success: (payload) => ({
    data: payload,
    datafield: "currentMovies",
    currentMovie: "top_rated",
  }),
});
export const nowPlayingMovies = createRoutine("NOW_PLAYING_MOVIES", {
  success: (payload) => ({
    data: payload,
    datafield: "currentMovies",
    currentMovie: "now_playing",
  }),
});
export const popularMovies = createRoutine("POPULAR_MOVIES", {
  success: (payload) => ({
    data: payload,
    datafield: "currentMovies",
    currentMovie: "popular",
  }),
});
export const trendingMovies = createRoutine("TRENDING_MOVIES", {
  success: (payload) => ({ data: payload, datafield: "trendingMovies" }),
});
export const upcomingMovies = createRoutine("UPCOMING_MOVIES", {
  success: (payload) => ({ data: payload, datafield: "upcomingMovies" }),
});
export const genres = createRoutine("GENRES", {
  success: (payload) => ({ data: payload, datafield: "genres" }),
});
export const recommendedMovies = createRoutine("RECOMMENDED_MOVIS", {
  success: (payload) => ({ data: payload, datafield: "recommendedMovies" }),
});
export const movieDetails = createRoutine("MOVIE_DETAILS", {
  success: (payload) => ({ data: payload, datafield: "movieDetails" }),
});
