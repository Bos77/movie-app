const initState = {
  isLoading: false,
  page: 1,
  currentMovieType: "popular",
  movieDetails: {},
  recommendedMovies: [],
  genres: [],
  currentMovies: [],
  trendingMovies: [],
  upcomingMovies: [],
  error: null,
};

export const apiReducer = (state = initState, { type, payload }) => {
  if (type.includes("/TRIGGER"))
    return { ...state, isLoading: true, page: payload.page };
  else if (type.includes("/SUCCESS")) {
    if (
      typeof payload.data === "object" &&
      !Array.isArray(payload.data) &&
      payload.data !== null
    ) {
      return {
        ...state,
        isLoading: false,
        [payload.datafield]: payload.data,
      };
    } else if (
      payload.hasOwnProperty("currentMovie") &&
      state.currentMovieType === payload.currentMovie
    ) {
      return {
        ...state,
        isLoading: false,
        [payload.datafield]: state[payload.datafield].concat(payload.data),
      };
    } else if (payload.hasOwnProperty("currentMovie")) {
      return {
        ...state,
        currentMovieType: payload.currentMovie,
        isLoading: false,
        [payload.datafield]: payload.data,
      };
    }
    return {
      ...state,
      isLoading: false,
      [payload.datafield]: state[payload.datafield].concat(payload.data),
    };
  } else if (type.includes("/FAILURE"))
    return { ...state, isLoading: false, error: payload };
  else return { ...state };
};
